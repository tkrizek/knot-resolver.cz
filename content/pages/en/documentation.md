title: Documentation
slug: documentation
menu: Documentation
lang: en
menuorder: 2

[![Documentation Status](https://readthedocs.org/projects/knot-resolver/badge/?version=stable)](https://knot-resolver.readthedocs.io/en/stable)

Docs are available online at [knot-resolver.readthedocs.org](http://knot-resolver.readthedocs.org/en/stable).

*Read the Docs* also offers downloads of [PDF](http://readthedocs.org/projects/knot-resolver/downloads/pdf/stable/), [HTML archive](http://readthedocs.org/projects/knot-resolver/downloads/htmlzip/stable/), and [EPUB](http://readthedocs.org/projects/knot-resolver/downloads/epub/stable/) formats for viewing offline.
