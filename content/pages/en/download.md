title: Downloads
slug: download
menu: Downloads
lang: en
menuorder: 2

## Official sources

- [Knot Resolver 1.5.1](https://secure.nic.cz/files/knot-resolver/knot-resolver-1.5.1.tar.xz) ([asc](https://secure.nic.cz/files/knot-resolver/knot-resolver-1.5.1.tar.xz.asc), [sha256](https://secure.nic.cz/files/knot-resolver/knot-resolver-1.5.1.tar.xz.sha256))
- [Knot Resolver 1.99.1-alpha](https://secure.nic.cz/files/knot-resolver/knot-resolver-1.99.1-alpha.tar.xz) ([asc](https://secure.nic.cz/files/knot-resolver/knot-resolver-1.99.1-alpha.tar.xz.asc), [sha256](https://secure.nic.cz/files/knot-resolver/knot-resolver-1.99.1-alpha.tar.xz.sha256))
- [Old releases](https://secure.nic.cz/files/knot-resolver/?C=N;O=D)

Tarballs for versions before 1.4.0 were signed by key:
```
DEF3 5D16 E5AE 59D8 20BD  F780 ACE2 4DA9 EE37 A832
```

Newer releases are signed with personal keys:
```
B600 6460 B60A 80E7 8206  2449 E747 DF1F 9575 A3AA
BE26 EBB9 CBE0 59B3 910C  A35B CE8D D6A1 A50A 21E4
```


## Official repositories

### Debian Jessie:

You need [jessie-backports](https://backports.debian.org/) and add both repositories to get libknot library used by Knot Resolver:

- [deb.knot-dns.cz/knot](https://deb.knot-dns.cz/knot/)
- [deb.knot-dns.cz/knot-resolver](https://deb.knot-dns.cz/knot-resolver/)

**run as root:**

```
apt-get install apt-transport-https wget
wget -O /etc/apt/trusted.gpg.d/knot.gpg https://packages.sury.org/knot/apt.gpg
wget -O /etc/apt/trusted.gpg.d/knot-resolver.gpg https://packages.sury.org/knot-resolver/apt.gpg
echo "deb http://ftp.debian.org/debian jessie-backports main" >> /etc/apt/sources.list.d/jessie-backports.list
echo "deb https://deb.knot-dns.cz/knot/ jessie main" > /etc/apt/sources.list.d/knot.list
echo "deb https://deb.knot-dns.cz/knot-resolver/ jessie main" > /etc/apt/sources.list.d/knot-resolver.list
apt-get update
apt-get install -t jessie-backports knot-resolver
```

### Debian Stretch:

You need to add both repositories to get libknot library used by Knot Resolver:

- [deb.knot-dns.cz/knot](https://deb.knot-dns.cz/knot/)
- [deb.knot-dns.cz/knot-resolver](https://deb.knot-dns.cz/knot-resolver/)

**run as root:**

```
apt-get install apt-transport-https wget
wget -O /etc/apt/trusted.gpg.d/knot.gpg https://packages.sury.org/knot/apt.gpg
wget -O /etc/apt/trusted.gpg.d/knot-resolver.gpg https://packages.sury.org/knot-resolver/apt.gpg
echo "deb https://deb.knot-dns.cz/knot/ stretch main" > /etc/apt/sources.list.d/knot.list
echo "deb https://deb.knot-dns.cz/knot-resolver/ stretch main" > /etc/apt/sources.list.d/knot-resolver.list
apt-get update
apt-get install knot-resolver
```

### Ubuntu PPA:

Again, you need both PPA repositories.

- [ppa:cz.nic-labs/knot-dns](https://launchpad.net/~cz.nic-labs/+archive/ubuntu/knot-dns/)
- [ppa:cz.nic-labs/knot-resolver](https://launchpad.net/~cz.nic-labs/+archive/ubuntu/knot-resolver/)

**run as root:**

```
LC_ALL=C.UTF-8 add-apt-repository ppa:cz.nic-labs/knot-dns
LC_ALL=C.UTF-8 add-apt-repository ppa:cz.nic-labs/knot-resolver
apt-get update
apt-get install knot-resolver
```

### Fedora:

Knot Resolver is available in the official Fedora repositories starting with Fedora 23.

**run as root:**

```
dnf install knot-resolver
```

### OpenSUSE:

Knot Resolver is available from the official [OpenSUSE repositories](https://software.opensuse.org/download.html?project=server%3Adns&package=knot-resolver) staring with OpenSUSE 42.1:

```
zypper addrepo http://download.opensuse.org/repositories/server:dns/openSUSE_42.1/server:dns.repo
zypper refresh
zypper install knot-resolver
```

## Building from sources

The Knot Resolver [depends][depends] on the 2.3.3 (and higher) version of the Knot DNS library, [LuaJIT][luajit] and [libuv][libuv].
See the [Building project][depends] documentation page for more information.

## Docker image

This is simple and doesn't require any dependencies or system modifications, just run:

```
$ docker run cznic/knot-resolver
```

See the build page [registry.hub.docker.com/r/cznic/knot-resolver](https://registry.hub.docker.com/r/cznic/knot-resolver) for more information and options.

## Running

The project builds a resolver library in the `lib` directory, and a daemon in the `daemon` directory.

```
$ kresd -h
```

See the documentation at [knot-resolver.readthedocs.org][doc].


[depends]: http://knot-resolver.readthedocs.org/en/latest/build.html
[doc]: http://knot-resolver.readthedocs.org/en/latest/index.html
[deckard]: https://gitlab.labs.nic.cz/knot/deckard
[luajit]: http://luajit.org/
[libuv]: https://github.com/libuv/libuv
