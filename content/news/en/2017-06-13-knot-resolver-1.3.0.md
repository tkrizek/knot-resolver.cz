title: Knot Resolver 1.3.0 released
slug: knot-resolver-1.3.0
date: 2017-06-13 09:00
lang: en

Knot Resolver 1.3.0 has been released.

Security
--------
- Refactor handling of AD flag and security status of resource records.
  In some cases it was possible for secure domains to get cached as
  insecure, even for a TLD, leading to disabled validation.
  It also fixes answering with non-authoritative data about nameservers.

Improvements
------------
- major feature: support for forwarding with validation (#112).
  The old policy.FORWARD action now does that; the previous non-validating
  mode is still avaliable as policy.STUB except that also uses caching (#122).
- command line: specify ports via @ but still support # for compatibility
- policy: recognize 100.64.0.0/10 as local addresses
- layer/iterate: *do* retry repeatedly if REFUSED, as we can't yet easily
  retry with other NSs while avoiding retrying with those who REFUSED
- modules: allow changing the directory where modules are found,
  and do not search the default library path anymore.

Bugfixes
--------
- validate: fix insufficient caching for some cases (relatively rare)
- avoid putting "duplicate" record-sets into the answer (#198)
