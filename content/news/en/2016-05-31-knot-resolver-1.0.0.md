title: Knot Resolver 1.0.0 released
slug: knot-resolver-1.0.0
date: 2016-05-30 10:10
lang: en

Knot Resolver 1.0.0 has been released.

The first production-ready release of Knot Resolver has been released.

The list of notable features:

- Full DNSSEC Support
- Automated Root Trust Anchor Rollover
- Negative Trust Anchors
- Light core extendable with plugins in C, Lua and Go
- QNAME Minimization
- DNS64 support
- Persistent caching - local or networked via Memcached or Redis
- Prefetching
- Views and ACL

For the full list of features, please see the [online documentation](https://knot-resolver.readthedocs.io/).
