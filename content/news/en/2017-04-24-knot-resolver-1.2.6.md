title: Knot Resolver 1.2.6 released
slug: knot-resolver-1.2.6
date: 2017-04-24 16:20
lang: en

Knot Resolver 1.2.6 has been released.

Security
--------
- dnssec: don't set AD flag for NODATA answers if wildcard non-existence
  is not guaranteed due to opt-out in NSEC3

Improvements
------------
- layer/iterate: don't retry repeatedly if REFUSED

Bugfixes
--------
- lib/nsrep: revert some changes to NS reputation tracking that caused
  severe problems to some users of 1.2.5 (#178 and #179)
- dnssec: fix verification of wildcarded non-singleton RRsets
- dnssec: allow wildcards located directly under the root
- layer/rrcache: avoid putting answer records into queries in some cases
