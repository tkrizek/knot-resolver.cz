title: Knot Resolver 1.1.1 released
slug: knot-resolver-1.1.1
date: 2016-08-24 14:00
lang: en

Knot Resolver 1.1.1 has been released.

This Knot Resolver release fixes couple of bugs related to failures when nameservers were not available and Knot Resolver had to retry with different IP addresses.

For the full list of features, please see the [online documentation](https://knot-resolver.readthedocs.io/).
