title: Knot Resolver 1.2.3 released
slug: knot-resolver-1.2.3
date: 2017-02-23 16:00
lang: en

Knot Resolver 1.2.3 has been released.

Bugfixes:
---------
* Disable storing GLUE records into the cache even in the (non-default) QUERY_PERMISSIVE mode
* iterate: skip answer RRs that don't match the query
* layer/iterate: some additional processing for referrals
* lib/resolve: zonecut fetching error was fixed
