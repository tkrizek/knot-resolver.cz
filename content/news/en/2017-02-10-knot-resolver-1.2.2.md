title: Knot Resolver 1.2.2 released
slug: knot-resolver-1.2.2
date: 2017-02-10 13:00
lang: en

Knot Resolver 1.2.2 has been released.

Bugfixes:
---------
* Fix -k argument processing to avoid out-of-bounds memory accesses
* lib/resolve: fix zonecut fetching for explicit DS queries
* hints: more NULL checks
* Fix TA bootstrapping for multiple TAs in the IANA XML file

Testing:
--------
* Update tests to run tests with and without QNAME minimization
