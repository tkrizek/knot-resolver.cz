title: Knot Resolver 1.5.1 released
slug: knot-resolver-1.5.1
date: 2017-12-12 14:00
lang: en


Incompatible changes
--------------------
- script supervisor.py was removed, please migrate to a real process manager
- module ketcd was renamed to etcd for consistency
- module kmemcached was renamed to memcached for consistency

Bugfixes
--------
- fix SIGPIPE crashes (#271)
- tests: work around out-of-space for platforms with larger memory pages
- lua: fix mistakes in bindings affecting 1.4.0 and 1.5.0 (and 1.99.1-alpha),
  potentially causing problems in dns64 and workarounds modules
- predict module: various fixes (!399)

Improvements
------------
- add priming module to implement RFC 8109, enabled by default (#220)
- add modules helping with system time problems, enabled by default;
  for details see documentation of detect_time_skew and detect_time_jump
