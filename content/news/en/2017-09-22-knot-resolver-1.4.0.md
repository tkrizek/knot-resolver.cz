title: Knot Resolver 1.4.0 released
slug: knot-resolver-1.4.0
date: 2017-09-22 12:00
lang: en

Knot Resolver 1.4.0 has been released.

Incompatible changes
--------------------
- lua: query flag-sets are no longer represented as plain integers.
  kres.query.* no longer works, and kr_query_t lost trivial methods
  'hasflag' and 'resolved'.
  You can instead write code like qry.flags.NO_0X20 = true.

Bugfixes
--------
- fix exiting one of multiple forks (#150)
- cache: change the way of using LMDB transactions.  That in particular
  fixes some cases of using too much space with multiple kresd forks (#240).

Improvements
------------
- policy.suffix: update the aho-corasick code (#200)
- root hints are now loaded from a zonefile; exposed as hints.root_file().
  You can override the path by defining ROOTHINTS during compilation.
- policy.FORWARD: work around resolvers adding unsigned NS records (#248)
- reduce unneeded records previously put into authority in wildcarded answers

