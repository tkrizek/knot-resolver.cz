title: Knot Resolver 1.3.3 released
slug: knot-resolver-1.3.3
date: 2017-08-09 12:00
lang: en

Knot Resolver 1.3.3 has been released.

Security
--------
- Fix a critical DNSSEC flaw.  Signatures might be accepted as valid
  even if the signed data was not in bailiwick of the DNSKEY used to
  sign it, assuming the trust chain to that DNSKEY was valid.

Bugfixes
--------
- iterate: skip RRSIGs with bad label count instead of immediate SERVFAIL
- utils: fix possible incorrect seeding of the random generator
- modules/http: fix compatibility with the Prometheus text format

Improvements
------------
- policy: implement remaining special-use domain names from RFC6761 (#205),
  and make these rules apply only if no other non-chain rule applies

