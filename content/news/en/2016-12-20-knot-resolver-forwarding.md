title: No DNSSEC when FORWARD policy enabled
slug: no-dnssec-with-forward-policy
date: 2016-12-20 14:00
lang: en

It was discovered that the documentation for
[FORWARD policy](http://knot-resolver.readthedocs.io/en/latest/modules.html#query-policies)
missed the information about DNSSEC validation.  The query policies
are only applied on inbound queries and therefore the full DNSSEC
validation is disabled when Knot Resolver operates in FORWARD mode,
and relies on DNSSEC validation from upstream resolver.

As a workaround, you can either:

 * Disable the FORWARD policy
 * Use upstream resolver with DNSSEC validation (f.e. [CZ.NIC ODVR](https://labs.nic.cz/odvr), [DNS-OARC ODVR](https://www.dns-oarc.net/oarc/services/odvr), [Verisign Public DNS](https://www.verisign.com/en_US/security-services/public-dns/index.xhtml), or [Google Public DNS](https://developers.google.com/speed/public-dns/) as a destination for FORWARD policy).

Example configuration:

```
-- Use IPv6 CZ.NIC Open DNSSEC Validating Resolvers
policy.add(policy.all(policy.FORWARD('2001:1488:800:400::130','2001:678:1::206')))
-- Use IPv4 CZ.NIC Open DNSSEC Validating Resolvers
policy.add(policy.all(policy.FORWARD('217.31.204.130','193.29.206.206')))

```

For the full list of Knot Resolver features, please see the [online documentation](https://knot-resolver.readthedocs.io/).
